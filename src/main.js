import { createApp } from 'vue'
import SearchMoteur from './SearchMoteur.vue'
import './assets/css/tailwind.css'
import './assets/css/main.scss'

const rootAppElement = document.getElementById('app')
let fields = {
}

const landing = !!rootAppElement.dataset.landing
// console.log(rootAppElement.dataset.fields)
// console.log(rootAppElement.dataset.landing)
// console.log(JSON.parse(rootAppElement.dataset.fields))

if(rootAppElement.dataset.fields) {
    try {
        fields = JSON.parse(rootAppElement.dataset.fields)
        // if(fields.localisation) fields.localisation = fields.localisation.split(',')
    } catch (e) {
        fields = {}
    }
}

createApp(SearchMoteur, {fields, landing}).mount('#app')
