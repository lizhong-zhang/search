/** @type {import('tailwindcss').Config} */
module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    fontSize: {
      xs: ['12px', '15px'],
      s: ['14px', '17.5px'],
      m: ['16px', '20px'],
      xm: ['18px', '22px'],
      l: ['22px', '28px'],
      xl: ['28px', '35px'],
      xxl: ['36px', '45px'],
      '3xl': ['52px', '65px'],
      '4xl': ['76px', '95px']
    },
    screens: {
      'xs': '325px',
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
    },
    extend: {
      colors: {
        primary: {
          light: '#F0761F',
          base: '#EC6608',
          dark: '#D14905'
        },
        secondary: {
          light: '#009FAD',
          base: '#163056',
          dark: '#01192F'
        },
        neutral: {
          'ultra-light': '#EBEDEE',
          light: '#AEB5BC',
          base: '#5C6C7A',
          dark: '#344758'
        },
        warning: {
          light: '#F9E9D2',
          base: '#D14905'
        }
      }
    },
  },
  plugins: [],
}
